package com.example.findapp

class Objet {

    var id : Int = 0
    var name: String = ""
    var desc: String = ""
    var img: String = ""
    var statut: Int = 0
    var location: String = ""
    var prix: Double=0.0

    constructor(name: String, desc: String, img: String, statut: Int, location: String){
        this.name=name
        this.desc=desc
        this.img=img
        this.statut=statut
        this.location=location

    }

    constructor(name:String, desc:String, img:String, prix:Double, location: String)
    {
        this.name=name
        this.desc=desc
        this.img=img
        this.prix=prix
        this.location=location
    }

    constructor(name:String, desc:String, img:String, prix:Double, location: String, statut:Int)
    {
        this.name=name
        this.desc=desc
        this.img=img
        this.prix=prix
        this.location=location
        this.statut=statut
    }


    constructor(){
        this.name=name
        this.desc=desc
        this.img=img
        this.statut=statut
        this.location=location
    }
}