package com.example.findapp


import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_fragment_ajout.*
import android.R.id
import android.support.v7.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_fragment_ob_vendre.*
import kotlinx.android.synthetic.main.logout_dialog.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentAjout : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_ajout, container, false)






    }

    override fun onStart() {
        super.onStart()

        btnPerteAjout.setOnClickListener{
            activity?.let{
                val intent = Intent (it, AjoutObjet::class.java)
                it.startActivity(intent)
            }
        }

        btnVenteAjout.setOnClickListener{
            activity?.let{
                val intent = Intent (it, AjoutVenteOb::class.java)
                it.startActivity(intent)
            }
        }

        btnDeconnexionAjout.setOnClickListener{
            val mDialogView = LayoutInflater.from(this.activity).inflate(R.layout.logout_dialog, null)

            val mBuilder = AlertDialog.Builder(this.activity!!).setView(mDialogView).setTitle("Déconnexion")

            mBuilder.show()

            mDialogView.btnPrincipal.setOnClickListener {
                activity?.let{
                    val intent = Intent (it, MainActivity::class.java)
                    it.startActivity(intent)
                }
            }
        }
    }





}



