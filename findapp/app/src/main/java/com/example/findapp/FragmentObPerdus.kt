package com.example.findapp


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_fragment_ajout.*
import kotlinx.android.synthetic.main.fragment_fragment_ob_perdus.*
import kotlinx.android.synthetic.main.logout_dialog.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentObPerdus : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_ob_perdus, container, false)
    }


    override fun onStart() {

        btnViewObjPerdu.setOnClickListener{
            activity?.let{
                val intent = Intent (it, ObjetsPerdus::class.java)
                it.startActivity(intent)
            }
        }

        btnAddObjPerdu.setOnClickListener {
            activity?.let{
                val intent = Intent (it, AjoutObjet::class.java)
                it.startActivity(intent)
            }
        }



        super.onStart()
    }


}
