package com.example.findapp

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_inscription.*

class InscriptionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inscription)

        var context = this
        var db = dbHandler(context)

        btnInscription.setOnClickListener ({
            if(etvNom.text.toString().length>0 && etvPrenom.text.toString().length>0
                && etvUsername.text.toString().length>0 && etvPassword.text.toString().length>0
                && etvEmail.text.toString().length>0 && etvTel.text.toString().length>0){
                var user = User(etvNom.text.toString(), etvPrenom.text.toString(), etvUsername.text.toString(),
                                etvPassword.text.toString(), etvEmail.text.toString(), etvTel.text.toString())
                if(db.seekUser(etvUsername.text.toString(), etvPassword.text.toString()).isEmpty()){
                    db.insertuserdata(user)
                    val alert = AlertDialog.Builder(this)

                    alert.setTitle("Inscription réussie!")
                    alert.setMessage("Bienvenue au monde des objets")
                    alert.setPositiveButton("OK"){
                        //cette ligne de code nous pernet de quitter l'activity et retourne au menu principal
                            dialogInterface: DialogInterface?, i: Int -> finish()
                    }
                    alert.show()
                }
                else{
                    Toast.makeText(this, "Désolé! Ce nom existe déjà!", Toast.LENGTH_SHORT).show()
                }

            }
            else{
                Toast.makeText(this, "Oups! Vous avez manqué un champ", Toast.LENGTH_SHORT).show()
            }
        })

        btnRetour.setOnClickListener({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        })
    }
}
