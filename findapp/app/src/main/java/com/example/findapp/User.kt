package com.example.findapp

class User {

    var id : Int = 0
    var name: String = ""
    var fname: String = ""
    var username: String = ""
    var password: String = ""
    var email: String = ""
    var tel: String = ""

    constructor(name: String, fname: String, username: String, password: String, email: String, tel: String){
        this.name=name
        this.fname=fname
        this.username=username
        this.password=password
        this.email=email
        this.tel=tel
    }

    constructor(username: String, password: String){
        this.username=username
        this.password=password
    }

    constructor(){
        this.name=name
        this.fname=fname
        this.username=username
        this.password=password
        this.email=email
        this.tel=tel
    }
}