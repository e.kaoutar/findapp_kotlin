package com.example.findapp


import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_fragment_ob_vendre.*
import kotlinx.android.synthetic.main.logout_dialog.*
import kotlinx.android.synthetic.main.logout_dialog.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentObVendre : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_ob_vendre, container, false)
    }

    override fun onStart() {

        btnAddVente.setOnClickListener {
            activity?.let{
                val intent = Intent (it, AjoutVenteOb::class.java)
                it.startActivity(intent)
            }
        }

        btnViewsaleobjects.setOnClickListener{
            activity?.let{
                val intent = Intent (it, ObjetsAvendre::class.java)
                it.startActivity(intent)
            }
        }

        //ALL GOOD HERE
        btnDeconnexion.setOnClickListener{
            val mDialogView = LayoutInflater.from(this.activity).inflate(R.layout.logout_dialog, null)

            val mBuilder = AlertDialog.Builder(this.activity!!).setView(mDialogView).setTitle("Déconnexion")

            val mAlertDialog = mBuilder.show()

            mDialogView.btnPrincipal.setOnClickListener {
                activity?.let{
                    val intent = Intent (it, MainActivity::class.java)
                    it.startActivity(intent)
                }
            }
        }

        super.onStart()
    }


}
