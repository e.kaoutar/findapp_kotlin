package com.example.findapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_objets_avendre.*
import kotlinx.android.synthetic.main.activity_objets_perdus.*

class ObjetsAvendre : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_objets_avendre)

        var context = this
        var db = dbHandler(context)

        var data = db.readdataVendu()
        tvResultVendu.text=""

        for( i in 0..data.size - 1){
            tvResultVendu.append("\nNom de l'objet: " + data.get(i).name+ "\n Description:  " + data.get(i).desc + "\n Prix: $"+ data.get(i).prix.toString()+" \n\n")
            tvResultVendu.append("---------------------------------------------------")
        }
    }
}
