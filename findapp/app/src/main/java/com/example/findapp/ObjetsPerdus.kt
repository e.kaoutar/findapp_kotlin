package com.example.findapp

import android.app.AlertDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_objets_perdus.*
import kotlinx.android.synthetic.main.deletedialog.view.*
import kotlinx.android.synthetic.main.editdialog.view.*

class ObjetsPerdus : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_objets_perdus)

        var context = this
        var db = dbHandler(context)

        var data = db.readdataPerdu()
        tvResultPerdu.text=""

        for( i in 0..data.size - 1){
            tvResultPerdu.append("\nNom de l'objet: " + data.get(i).name+ "\n Description:  " + data.get(i).desc + "\n\n")
            tvResultPerdu.append("---------------------------------------------------")
        }

        btnSupprime.setOnClickListener {
            val mDialogViewDelete = LayoutInflater.from(this).inflate(R.layout.deletedialog, null)

            val mBuilder = AlertDialog.Builder(this).setView(mDialogViewDelete).setTitle("Suppression")

            mBuilder.show()

            mDialogViewDelete.btnDeleteDialog.setOnClickListener {

                //requete de suppression
                var nameOb = mDialogViewDelete.etvnameObj.text.toString()
                db.deleteObject(nameOb)
                Toast.makeText(this, "Objet supprimé", Toast.LENGTH_SHORT).show()

            }

            mDialogViewDelete.btnCancel.setOnClickListener {
                val intent = Intent(this, ObjetsPerdus::class.java)
                startActivity(intent)
            }

        }

        btnRefresh.setOnClickListener {
            var data = db.readdataPerdu()
            tvResultPerdu.text=""

            for( i in 0..data.size - 1){
                tvResultPerdu.append("\nNom de l'objet: " + data.get(i).name+ "\n Description:  " + data.get(i).desc + "\n\n")
                tvResultPerdu.append("---------------------------------------------------")
            }
        }

        btnModifier.setOnClickListener {
            val mDialogViewUpdate= LayoutInflater.from(this).inflate(R.layout.editdialog, null)

            val mBuilder = AlertDialog.Builder(this).setView(mDialogViewUpdate).setTitle("Suppression")

            mBuilder.show()

            mDialogViewUpdate.btnEditDialog.setOnClickListener {

                //requete de suppression
                var oldName = mDialogViewUpdate.etvnameObjToEdit.text.toString()
                var newName = mDialogViewUpdate.etvnameObjEdited.text.toString()
                var newDesc = mDialogViewUpdate.etvnewDesc.text.toString()
                db.editObject(oldName, newName, newDesc)
                Toast.makeText(this, "Objet modifié avec succès", Toast.LENGTH_SHORT).show()

            }

            mDialogViewUpdate.btnCancelEdit.setOnClickListener {
                val intent = Intent(this, ObjetsPerdus::class.java)
                startActivity(intent)
            }
        }


    }





}
