package com.example.findapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    //objet global : reutilisable dans menu activity
    companion object {
        const val LEUSER = "leuser"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        var context = this
        var db = dbHandler(context)


        btnConnexion.setOnClickListener({
            if(etvUsername.text.length>0 && etvPassword.text.length>0){
                if(db.seekUser(etvUsername.text.toString(), etvPassword.text.toString()) != null){

                    var leuser = User(etvUsername.text.toString(), etvPassword.text.toString())
                    var usr = UserParcel(leuser.username, leuser.password, leuser.name, leuser.fname, leuser.email, leuser.tel)
                    val intent = Intent(this, MenuActivity::class.java)
                    intent.putExtra(LEUSER, usr)
                    startActivity(intent)
                }
                else{
                    Toast.makeText(this, "Impossible de trouver ce compte", Toast.LENGTH_SHORT).show()
                }
            }
            else{
                Toast.makeText(this, "Oups! Un de vos champs est vide.", Toast.LENGTH_SHORT).show()
            }
        })

        btnRetour.setOnClickListener({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        })
    }
}
