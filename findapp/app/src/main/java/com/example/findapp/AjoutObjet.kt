package com.example.findapp

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.util.Log
import android.webkit.PermissionRequest
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_ajout_objet.*
import kotlinx.android.synthetic.main.activity_inscription.*
import java.util.jar.Manifest

class AjoutObjet : AppCompatActivity() {

    private val PERMISSION_CODE = 100
    var image_rui : Uri? = null
    private val IMAGE_CAPTURE_CODE = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ajout_objet)


        var context = this
        var db = dbHandler(context)

        btnSave.setOnClickListener {
            if (etvNameObj.text.length > 0 && etvDesc.text.length > 0) {
                var ob = Objet(
                    etvNameObj.text.toString(), etvDesc.text.toString(), image_view.toString(), 1, "Montreal, Qc")
                db.insertobjectdata(ob)

                Toast.makeText(this, "Objet ajouté!", Toast.LENGTH_SHORT).show()
                clearAll()
            } else {
                Toast.makeText(this, "Oups! Vous avez manqué un champ", Toast.LENGTH_SHORT).show()
            }
        }

        btnAddPhoto.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    //permission not enabled
                    val permission =
                        arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE)
                } else {
                    //permission already granted
                    openCamera()
                }
            } else {
                //system os is < marshmallow
                openCamera()
            }
        }

        btnReturn.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
            clearAll()
        }
    }

    private fun clearAll(){
        etvNameObj.text.clear()
        etvDesc.text.clear()
        image_view.clearFocus()
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_rui = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_rui)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from permission request popup

        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.size>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission from popup has been granted
                    openCamera()
                }
                else{
                    //permission from popup has been denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()

                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //called when image was captured from camera intent
        if(resultCode == Activity.RESULT_OK){
            //set image captured to image view
            image_view.setImageURI(image_rui)

        }
    }

}








