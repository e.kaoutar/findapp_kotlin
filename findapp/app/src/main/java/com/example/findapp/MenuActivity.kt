package com.example.findapp

import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.fragment_fragment_ajout.*
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_profil.*
import kotlinx.android.synthetic.main.fragment_fragment_objets.*
import kotlinx.android.synthetic.main.logout_dialog.view.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        btnDeconnexion.setOnClickListener{
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.logout_dialog, null)

            val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setTitle("Déconnexion")

            mBuilder.show()

            mDialogView.btnPrincipal.setOnClickListener {

                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
        }

        btnobjPerdus.setOnClickListener {
            var intent = Intent(this, ObjetsPerdus::class.java)
            startActivity(intent)
        }

        btnaddObjPerdu.setOnClickListener {
            var intent = Intent(this, AjoutObjet::class.java)
            startActivity(intent)
        }

        btnaddObjVente.setOnClickListener {
            var intent = Intent(this, AjoutVenteOb::class.java)
            startActivity(intent)
        }

        btnObjVendus.setOnClickListener {
            var intent = Intent(this, ObjetsAvendre::class.java)
            startActivity(intent)
        }

    }

}
