package com.example.findapp

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.lang.Exception


//fondamentals
val database_name = "findApp"
val table_name_users = "Users"
val table_name_objets = "Objets"

//-------------------------------------
val col_ID_user = "IDU"
val col_name_user = "Name"
val col_fname_user = "Firstname"
val col_password = "Password"
val col_username = "Username"
val col_email = "Email"
val col_tel = "Telephone"
//-------------------------------------
val col_ID_objet = "IDO"
val col_name_objet = "Nameobject"
val col_Description = "Description"
val col_Srcimg = "Src"
val col_Statut = "Statut"
val col_Localisation = "Localisation"
//-------------------------------------



class dbHandler(context: Context) : SQLiteOpenHelper(context, database_name, null, 1) {

    override fun onCreate(db: SQLiteDatabase?) {
        val createTableUser = "CREATE TABLE "+ table_name_users + "( " + col_ID_user + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ col_name_user + " VARCHAR(25), "+ col_fname_user +" VARCHAR(25), "+col_password+" VARCHAR(25), "+col_username+" VARCHAR(25), "+col_email+" varchar(50), "+col_tel+" varchar(20));"
        db?.execSQL(createTableUser)

        val createTableObject= "CREATE TABLE "+ table_name_objets + " ( " + col_ID_objet + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ col_name_objet + " VARCHAR(25), "+ col_Description +" VARCHAR(100), "+col_Srcimg+" VARCHAR(50), "+col_Statut+" INTEGER, "+col_Localisation+" varchar(50));"
        db?.execSQL(createTableObject)


        val createTableObjetVendus = "CREATE TABLE ObjetsVendus(Idvo INTEGER PRIMARY KEY AUTOINCREMENT, NameOb VARCHAR(25), DescOb VARCHAR(55), ImgSrcv VARCHAR(25), Prix DOUBLE, Localisationv VARCHAR(25));"
        db?.execSQL(createTableObjetVendus)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    //insert user function
    fun insertuserdata(user: User){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(col_name_user, user.name)
        cv.put(col_fname_user, user.fname)
        cv.put(col_username, user.username)
        cv.put(col_password, user.password)
        cv.put(col_email, user.email)
        cv.put(col_tel, user.tel)
        db.insert(table_name_users, null, cv)
    }

    //login
    fun seekUser(username: String, password: String) : MutableList<User>{
        var list : MutableList<User> = ArrayList()

        var db = this.readableDatabase
        val query = "Select * from " + table_name_users + " where "+ col_username+" = '"+username+"' and "+ col_password+" = '"+password+"' ;"
        var result = db.rawQuery(query, null)
        if(result.moveToFirst()){
            do{
                var user = User()
                user.id = result.getString(result.getColumnIndex(col_ID_user)).toInt()
                user.name = result.getString(result.getColumnIndex(col_name_user))
                user.fname = result.getString(result.getColumnIndex(col_fname_user))
                user.username = result.getString(result.getColumnIndex(col_username))
                user.password = result.getString(result.getColumnIndex(col_password))
                user.email = result.getString(result.getColumnIndex(col_email))
                user.tel = result.getString(result.getColumnIndex(col_tel))
                list.add(user)
            }while(result.moveToNext())
        }

        return list
    }

    //insert object function
    fun insertobjectdata(objet: Objet){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(col_name_objet, objet.name)
        cv.put(col_Description, objet.desc)
        cv.put(col_Srcimg, objet.img)
        cv.put(col_Statut, objet.statut)
        cv.put(col_Localisation, objet.location)
        db.insert(table_name_objets, null, cv)
    }

    //insert objectV function
    fun insertobjectVdata(objet: Objet){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put("NameOb", objet.name)
        cv.put("DescOb", objet.desc)
        cv.put("ImgSrcv", objet.img)
        cv.put("Prix", objet.prix)
        cv.put("Localisationv", objet.location)
        db.insert("ObjetsVendus", null, cv)
    }

    //read objects
    fun readdataPerdu() : MutableList<Objet>{
        var list : MutableList<Objet> = ArrayList()

        var db = this.readableDatabase
        val query = "Select * From $table_name_objets"
        var result = db.rawQuery(query, null)
        if(result.moveToFirst()){
            do{
                var obj = Objet()
                obj.id= result.getString(result.getColumnIndex(col_ID_objet)).toInt()
                obj.name= result.getString(result.getColumnIndex(col_name_objet))
                obj.desc= result.getString(result.getColumnIndex(col_Description))
                obj.img= result.getString(result.getColumnIndex(col_Srcimg))
                obj.location= result.getString(result.getColumnIndex(col_Localisation))
                obj.statut = result.getString(result.getColumnIndex(col_Statut)).toInt()
                list.add(obj)
            }while(result.moveToNext())
        }

        return list
    }

    //read objects vendus
    fun readdataVendu() : MutableList<Objet>{
        var list : MutableList<Objet> = ArrayList()

        var db = this.readableDatabase
        val query = "Select * From  ObjetsVendus"
        var result = db.rawQuery(query, null)
        if(result.moveToFirst()){
            do{
                var obj = Objet()
                obj.id= result.getString(result.getColumnIndex("Idvo")).toInt()
                obj.name= result.getString(result.getColumnIndex("NameOb"))
                obj.desc= result.getString(result.getColumnIndex("DescOb"))
                obj.img= result.getString(result.getColumnIndex("ImgSrcv"))
                obj.location= result.getString(result.getColumnIndex("Localisationv"))
                obj.prix = result.getString(result.getColumnIndex("Prix")).toDouble()
                list.add(obj)
            }while(result.moveToNext())
        }

        return list
    }

    //read object
    fun deleteObject(objName : String):Boolean{
        val qry = "DELETE FROM $table_name_objets WHERE $col_name_objet = '"+objName+"'"
        val db = this.writableDatabase
        var result : Boolean = false

        try{
            val cursor = db.execSQL(qry)
            result=true
        }
        catch(e: Exception){
            Log.e(ContentValues.TAG, "Erreur de suppression")
        }
        db.close()
        return result
    }

    //Edit table objects
    fun editObject(oldName:String, newName:String, newDesc:String):Boolean{
        val db = this.writableDatabase
        var result = false
        val content=ContentValues()
        content.put(col_name_objet, newName)
        content.put(col_Description, newDesc)
        try{
            db.update(table_name_objets, content, "$col_name_objet=?", arrayOf(oldName))
            result=true
        }
        catch(e: Exception){
            Log.e(ContentValues.TAG, "Erreur de modification")
        }
        db.close()
        return result
    }

}